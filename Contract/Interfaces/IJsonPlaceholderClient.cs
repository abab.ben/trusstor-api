﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contract.Interfaces
{
    public interface IjsonPlaceholderClient
    {
        Task<IEnumerable<Photo>> GetPhotos();
        Task<IEnumerable<Comment>> GetComments();
    }
}
