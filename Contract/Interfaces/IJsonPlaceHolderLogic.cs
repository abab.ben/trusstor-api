﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contract.Interfaces
{
    public interface IJsonPlaceHolderLogic
    {
        Task<IEnumerable<Photo>> GetPhotosLogic();
        Task<IEnumerable<string>> GetUsers();
        Task<IEnumerable<Comment>> GetComments();
    }
}
