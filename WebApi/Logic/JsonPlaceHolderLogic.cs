﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contract;
using Contract.Interfaces;
using Repository;

namespace Logic
{
    public class JsonPlaceHolderLogic : IJsonPlaceHolderLogic
    {
        private readonly IjsonPlaceholderClient _jsonPlaceholderClient;
        private int photosIndex = 0;
        private int firstChunck = 120;
        private int otherChuncks = 60;

        public JsonPlaceHolderLogic(IjsonPlaceholderClient jsonPlaceholderClient)
        {
            _jsonPlaceholderClient = jsonPlaceholderClient;
        }

        public async Task<IEnumerable<Photo>> GetPhotosLogic()
        {
            var photos = (await _jsonPlaceholderClient.GetPhotos()).ToList();
            if (photosIndex == 0)
            {
                photosIndex = 120;
                return photos.GetRange(0, firstChunck);
            }
            else
            {
                var endPhotoIndex = photosIndex + 60;
                if (photos.Count <= endPhotoIndex)
                {
                    photosIndex = photos.Count - 1;
                }
                else endPhotoIndex = photosIndex + 60;
                return photos.GetRange(photosIndex, endPhotoIndex);
            }
        }

        public async Task<IEnumerable<Comment>> GetComments(string user)
        {
            var comments = await _jsonPlaceholderClient.GetComments();
            var userComments = comments.ToList().Where(item => item.email == user);
            if (!userComments.Any()) return null;

            return userComments;
        }

        public async Task<IEnumerable<string>> GetUsers()
        {
            var comments = await _jsonPlaceholderClient.GetComments();
            return comments.Select(item => item.email);
        }
    }
}
