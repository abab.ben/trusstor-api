﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contract;
using Contract.Interfaces;
using Logic;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class TrusstorController : ControllerBase
    {
        public readonly IJsonPlaceHolderLogic _jsonPlaceHolderLogic;

        public TrusstorController(IJsonPlaceHolderLogic jsonPlaceHolderLogic)
        {
            _jsonPlaceHolderLogic = jsonPlaceHolderLogic;
        }

        // GET api/values
        [HttpGet]
        [Route("photos")]
        public async Task<IEnumerable<Photo>> GetPhotosAsync()
        {
            return await _jsonPlaceHolderLogic.GetPhotosLogic();
        }

        [HttpGet]
        [Route("users")]
        public async Task<IEnumerable<string>> GetUsersAsync()
        {
            return await _jsonPlaceHolderLogic.GetUsers();
        }

        [HttpGet]
        [Route("comments")]
        public async Task<IEnumerable<Comment>> GetCommentsAsync(string user)
        {
            return await _jsonPlaceHolderLogic.GetComments(user);
        }
    }
}
