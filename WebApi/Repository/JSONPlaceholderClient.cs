﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Contract;
using Contract.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace Repository
{
    public class JsonPlaceholderClient : IjsonPlaceholderClient
    {
        private readonly string _jsonPlaceHolderUrl = "https://jsonplaceholder.typicode.com";
        private readonly IMemoryCache _memoryCache;

        public JsonPlaceholderClient(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<Photo>> GetPhotos()
        {
            //var photos = await Get<Photo>("photos");
            //_memoryCache.Set("photos", photos);
            return await _memoryCache.GetOrCreateAsync<IEnumerable<Photo>>("photos",
                cacheEntry => { return Get<Photo>("photos"); });
        }

        public async Task<IEnumerable<Comment>> GetComments()
        {
            return await _memoryCache.GetOrCreateAsync<IEnumerable<Comment>>("comments",
                cacheEntry => { return Get<Comment>("comments"); });
        }


        private async Task<IEnumerable<T>> Get<T>(string requestUri)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{_jsonPlaceHolderUrl}");
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = await client.GetAsync(requestUri);
                    var content = await response.Content.ReadAsStringAsync();
                    var convertedResponse = JsonConvert.DeserializeObject<IEnumerable<T>>(content);
                    return convertedResponse;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
