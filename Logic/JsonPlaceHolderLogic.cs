﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contract;
using Contract.Interfaces;
using Repository;

namespace Logic
{
    public class JsonPlaceHolderLogic : IJsonPlaceHolderLogic
    {
        private readonly IjsonPlaceholderClient _jsonPlaceholderClient;

        public JsonPlaceHolderLogic(IjsonPlaceholderClient jsonPlaceholderClient)
        {
            _jsonPlaceholderClient = jsonPlaceholderClient;
        }

        public async Task<IEnumerable<Photo>> GetPhotosLogic()
        {
            return await _jsonPlaceholderClient.GetPhotos();
        }

        public async Task<IEnumerable<Comment>> GetComments()
        {
            return await _jsonPlaceholderClient.GetComments();
        }

        public async Task<IEnumerable<string>> GetUsers()
        {
            var comments = await _jsonPlaceholderClient.GetComments();
            return comments.Select(item => item.email);
        }
    }
}
